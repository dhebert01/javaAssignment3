package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void getValueFromEcho_return5(){
        App a = new App();
        assertEquals(5, a.echo(5));
    }
     @Test
    public void getValueFromOneMore_return6(){
        App a = new App();
        assertEquals(6, a.oneMore(5));
    }
}
